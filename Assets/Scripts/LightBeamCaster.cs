﻿using UnityEngine;
using System.Collections;

public class LightBeamCaster : MonoBehaviour {

	public bool prismHit = false;

	// Use this for initialization
	void Start () 
	{
		prismHit = false;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKey (KeyCode.Space) && !(Input.GetKey (KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow)))
		{
			if (!prismHit)
			{
				if (this.transform.localScale.x + Time.deltaTime * 50 <= 30) 
				{
					this.transform.localScale += new Vector3 (Time.deltaTime * 50, 0, 0);
				}
			}
		}
		else
		{
			if (this.transform.localScale.x - Time.deltaTime*250 >= 0)
			{
				this.transform.localScale -= new Vector3(Time.deltaTime*200,0,0);
			}
		}
	}
}
