﻿using UnityEngine;
using System.Collections;

public class FinalBossScript : MonoBehaviour {

	public float shotFrequency;
	public int lives;
	public float speed;
	public bool goToFinal;

	public Vector3 initialPosition;
	public Vector3 finalPosition;

	public GameObject[] shots;

	// Use this for initialization
	void Awake () {

		initialPosition = transform.position;

		finalPosition = new Vector3 (transform.position.x,
		                             transform.position.y - 2,
		                             transform.position.z);

	}
	
	// Update is called once per frame
	void FixedUpdate () {

		Movement ();

		if (Time.timeSinceLevelLoad % shotFrequency == 0) {
			ShotTheGun();
		}
	
	}

	private void ShotTheGun(){

		foreach(GameObject shot_gun in shots ){
			shot_gun.GetComponent<ShotScript> ().is_shot = true;
			shot_gun.GetComponent<ShotScript> ().initialPosition = transform.position;
			shot_gun.GetComponent<ShotScript> ().BackToInitialPosition ();
		}


	}

	private void Movement(){

		if(goToFinal){

			transform.position = Vector3.Lerp (transform.position,
			                                   finalPosition,
			                                   Time.deltaTime * speed);
			if(Vector3.Distance(transform.position, finalPosition) < 0.1f){
				goToFinal = false;
			}
		}
		else{
			transform.position = Vector3.Lerp (transform.position,
			                                   initialPosition,
			                                   Time.deltaTime * speed);
			if(Vector3.Distance(transform.position, initialPosition) < 0.1f){
				goToFinal = true;
			}
		}

	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.gameObject.tag == "Spaceship_Shot") {
			lives--;
			if(lives == 0){
				Application.LoadLevel ("2_CinematicReality");
				// Game Over
			}
		}
		
	}

}
