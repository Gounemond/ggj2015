﻿using UnityEngine;
using System.Collections;

public class SpeceshipController : MonoBehaviour {

	public float speed;
	public int lives;
	public GameObject[] shots;

	private int counter;

	// Use this for initialization
	void Awake () {

		counter = 0;

	}
	
	// Update is called once per frame
	void Update () {
	
		if(Input.GetKey(KeyCode.RightArrow)){
			transform.Translate(Vector3.down * speed * Time.deltaTime);
		}
		if(Input.GetKey(KeyCode.LeftArrow)){
			transform.Translate(Vector3.up * speed * Time.deltaTime);
		}

		if (Input.GetKeyDown (KeyCode.Space)) {
			ShotTheGun();
		}

	}

	private void ShotTheGun(){
		shots [counter].GetComponent<ShotScript> ().is_shot = true;
		shots[counter].GetComponent<ShotScript> ().initialPosition = transform.position;
		shots[counter].GetComponent<ShotScript> ().BackToInitialPosition ();
		counter++;
		if (counter == shots.Length) {
			counter = 0;
		}
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.gameObject.tag == "Final_Boss_Shot") {
			lives--;
			if(lives == 0)
			{
				Application.LoadLevel ("2_CinematicReality");
				// Game Over
			}
		}
		
	}
}
