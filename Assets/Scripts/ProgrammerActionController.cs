﻿using UnityEngine;
using System.Collections;

public class ProgrammerActionController : MonoBehaviour {

	public bool activePower;

	// Use this for initialization
	void Start () {
		activePower = false;
	}
	
	// Update is called once per frame
	void Update () {
	
		if(Input.GetKeyDown(KeyCode.Space)){
			activePower = true;
		}

		if(Input.GetKeyUp(KeyCode.Space)){
			activePower = false;
		}

	}
}
