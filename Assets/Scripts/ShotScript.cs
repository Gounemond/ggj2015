﻿using UnityEngine;
using System.Collections;

public class ShotScript : MonoBehaviour {

	public float speed;
	public bool finalBoss;
	public Vector3 initialPosition;
	public bool is_shot;
	public GameObject father;

	// Use this for initialization
	void Awake () {

		if(finalBoss){
			speed = 5f;
		}else{
			speed = 10f;
		}

		initialPosition = transform.position;

		is_shot = false;

	}
	
	// Update is called once per frame
	void Update () {
	
		if (is_shot) {
			if(finalBoss){
				transform.Translate (speed * Vector3.left * Time.deltaTime);
			}
			else{
				transform.Translate (speed * Vector3.right * Time.deltaTime);
			}
		}
		else{
			transform.position = father.transform.position;
		}

	}

	public void BackToInitialPosition(){
		transform.position = initialPosition;
	}


}
