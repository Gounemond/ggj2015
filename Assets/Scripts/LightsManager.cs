﻿using UnityEngine;
using System.Collections;

public class LightsManager : MonoBehaviour {

	public Light[] lights;

	public Light cristallone;
	public float max_intensity;
	public float speed;

	public GameObject blockPath;
	public GameObject bridge;

	// Use this for initialization
	void Start () {
	
		bridge.SetActive (false);

	}
	
	// Update is called once per frame
	void Update () {

		bool allOk = true;

		foreach(Light oneLight in lights){

			if(oneLight.intensity < 4){
				allOk = false;
				break;
			}
		}

		if (allOk) {
			EnlightLight();
		}
		else{
			cristallone.intensity = 0;
		}
	
	}

	public void EnlightLight(){
		if (cristallone.intensity <= max_intensity) {
			
			cristallone.intensity += Time.deltaTime * speed;
			
		}
		else{
			bridge.SetActive(true);
			blockPath.SetActive(false);
		}
	}

	public void DarkAllLights(){
		foreach(Light oneLight in lights){
			oneLight.intensity = 0;
		}
	}
}
