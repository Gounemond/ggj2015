﻿using UnityEngine;
using System.Collections;

public class BreakpointBehaviour : MonoBehaviour {


	void Awake(){
		gameObject.GetComponent<SpriteRenderer> ().enabled = false;
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.GetComponent<ProgrammerActionController> () != null) {
			if(other.GetComponent<ProgrammerActionController> ().activePower){
				gameObject.GetComponent<SpriteRenderer> ().enabled = true;
			}
		}

	}
}
