﻿using UnityEngine;
using System.Collections;

public class Controller2D : MonoBehaviour {

	public float movementSpeed;
	public float rotationSpeed;
	public Vector3 movementDirection;
	public Vector2 respawnPoint;

	// Use this for initialization
	void Start () {
	
		movementSpeed = 3f;
		rotationSpeed = 200f;

	}
	
	// Update is called once per frame
	void Update () {

		if(Input.GetKey(KeyCode.UpArrow)){
			this.transform.Translate(Vector3.right * Time.deltaTime * movementSpeed);
		}

		if(Input.GetKey(KeyCode.DownArrow)){
			this.transform.Translate(Vector3.left * Time.deltaTime * movementSpeed);
		}

		if (Input.GetKey (KeyCode.RightArrow)) {
			this.transform.Rotate(Vector3.back * Time.deltaTime * rotationSpeed);
		}
	

		if (Input.GetKey (KeyCode.LeftArrow)) {
			this.transform.Rotate(Vector3.forward * Time.deltaTime * rotationSpeed);
		}

	}

	public void Respawn(){
		gameObject.transform.position = respawnPoint;
	}
}
