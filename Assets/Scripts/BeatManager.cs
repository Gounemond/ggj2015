﻿using UnityEngine;
using System.Collections;

public class BeatManager : MonoBehaviour {

	public float difficulty;
	public float beat_ratio;
	public bool get_it;
	public bool enter_control;
	public bool enter_control_1;
	public GameObject[] all_plats;



	// Use this for initialization
	void Start () {
	
		get_it = false;
		enter_control = false;
		enter_control_1 = true;

	}
	
	// Update is called once per frame
	void FixedUpdate () {
	
		if (Time.time % beat_ratio >= beat_ratio - difficulty*2 ||
		    Time.time % beat_ratio <= difficulty) {

			if(!enter_control_1){
				enter_control_1 = true;
				enter_control = false;
				get_it = false;
			}

			if(Input.GetKeyDown(KeyCode.Space)){
				get_it = true;
				// Move
				foreach(GameObject plat in all_plats){
					plat.GetComponent<PlatBehaviour>().ChangePosition();
				}
			}

		}
		else{
			if(!enter_control){
				enter_control = true;
				enter_control_1 = false;
				if(!get_it){
					foreach(GameObject plat in all_plats){
						plat.GetComponent<PlatBehaviour>().counter = 0;
						plat.GetComponent<PlatBehaviour>().ChangePosition();
					}
				}
			}
			if(Input.GetKeyDown(KeyCode.Space)){
				// Change to 0 for every one
				// Move
				foreach(GameObject plat in all_plats){
					plat.GetComponent<PlatBehaviour>().counter = 0;
					plat.GetComponent<PlatBehaviour>().ChangePosition();
				}
			}

		}
	}

}
