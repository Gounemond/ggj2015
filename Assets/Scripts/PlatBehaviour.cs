﻿using UnityEngine;
using System.Collections;


public class PlatBehaviour : MonoBehaviour {

	public GameObject[] target;
	public float difficulty;
	public float beat_ratio;
	public int beats_number;
	public float speed;
	
	private float distance;
	private float distance_on_division;
	public int counter;
	private float translation_time;
	private bool inMove;
	private Vector3 initialPosition;
	private GameObject beat_manager;



	// Use this for initialization
	void Start () {
		
		distance = Vector3.Distance (target[counter].transform.position, transform.position);
		distance_on_division = distance / beats_number;

		initialPosition = transform.position;

		beat_manager = GameObject.Find ("Beat_Manager");

		inMove = false;

	}
	
	// Update is called once per frame
	void Update () {
	
		if (inMove) {
			ChangePosition ();
		}


	}

	public void ChangePosition(){
		inMove = true;
		transform.position = Vector3.Lerp (transform.position, target[counter].transform.position, Time.deltaTime * speed);
		if(Vector3.Distance(transform.position, target[counter].transform.position) < 0.1){
			inMove = false;
			counter++;
			if(counter == target.Length){
				counter -= 1;
			}
		}
	}
}
