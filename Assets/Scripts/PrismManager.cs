﻿using UnityEngine;
using System.Collections;

public class PrismManager : MonoBehaviour 
{

	public LightBeamCaster beamCaster;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D ( Collider2D other)
	{
		Debug.Log ("BorgoDi");

		beamCaster.prismHit = true;
	}

	void OnTriggerExit2D (Collider2D other )
	{
		beamCaster.prismHit = false;
	}
}
