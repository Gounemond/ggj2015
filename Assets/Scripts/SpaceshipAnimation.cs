﻿using UnityEngine;
using System.Collections;

public class SpaceshipAnimation : MonoBehaviour {

	public Sprite[] sprites_normal;
	public Sprite[] sprites_hit;
	public float frame_per_seconds;

	private bool normal;
	private bool onLoop;
	
	private SpriteRenderer spriteRender;

	// Use this for initialization
	void Awake () {
	
		normal = true;
		onLoop = false;
		spriteRender = gameObject.GetComponent<SpriteRenderer> ();

	}
	
	// Update is called once per frame
	void Update () {
	
		if(normal){
			int index = (int)(Time.timeSinceLevelLoad * frame_per_seconds);
			index = index % sprites_normal.Length;
			spriteRender.sprite = sprites_normal[index];
		}
		else{
			if(!onLoop){
				onLoop = true;
				int index = (int)(Time.timeSinceLevelLoad * frame_per_seconds);
				index = index % sprites_hit.Length;
				spriteRender.sprite = sprites_hit[index];
			}
			else{
				int index = (int)(Time.timeSinceLevelLoad * frame_per_seconds);
				index = index % sprites_hit.Length;
				spriteRender.sprite = sprites_hit[index];
				if(index == 0){
					normal = true;
					onLoop = false;
				}
			}
		}

	}

	void OnTriggerEnter2D(Collider2D other){
		normal = false;
	}
}
