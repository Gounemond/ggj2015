﻿using UnityEngine;
using System.Collections;

public class PitfallBehaviour : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.GetComponent<Controller2D>() != null){

			other.GetComponent<Controller2D>().Respawn();
		}
	}

}
