﻿using UnityEngine;
using System.Collections;

public class LightEvilScript : MonoBehaviour {

	
	public Light light;
	public float speed;
	public float max_intensity;
	
	public bool isHit;
	//	public bool isActive;
	
	// Use this for initialization
	void Start () {
		
		light = GetComponent<Light> ();
		isHit = false;
		//		isActive = false;
		
	}
	
	// Update is called once per frame
	void Update () {
		
		if(isHit){
			EnlightLight();
		}
		else{
			if(light.intensity < max_intensity){
				DarkLight();
			}
		}
		
	}
	
	void OnTriggerEnter2D(Collider2D other){
		isHit = true;
	}
	
	void OnTriggerExit2D(Collider2D other){
		isHit = false;
	}
	
	public void EnlightLight(){
		if (light.intensity <= max_intensity) {
			
			light.intensity += Time.deltaTime * speed;
			
		}
		else{
			light.intensity = 0f;
			GameObject.Find("Lights_Manager").GetComponent<LightsManager>().DarkAllLights();
		}
	}
	
	public void DarkLight(){
		if (light.intensity >= 0) {
			
			light.intensity -= Time.deltaTime * speed;
			
		}
	}
}
