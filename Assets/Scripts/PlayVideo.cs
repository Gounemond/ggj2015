﻿using UnityEngine;
using System.Collections;

public class PlayVideo : MonoBehaviour 
{
	MovieTexture movie;
	public string nextLevel;

	// Use this for initialization
	void Start () 
	{
	    // Simple as eating bread! Get the movietexture from the renderer
		// and simply Play()
		movie = renderer.material.mainTexture as MovieTexture;
		movie.Play ();
	}
	
	// Update is called once per frame
	void Update () 
	{
	    // If the user press a key to skip the video, or the video finishes, switch to next scene
		if (Time.time > movie.duration || Input.GetKey(KeyCode.Return) || Input.GetKey(KeyCode.Escape) || Input.GetKey(KeyCode.Space))
		{
			Application.LoadLevel (nextLevel);
		}	
	}
}
